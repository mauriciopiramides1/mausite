---
title: "The_Office"
date: 2021-10-24T19:12:45-03:00
draft: false
---

![The_office](https://rollingstone.uol.com.br/media/_versions/office_reproducao_corte_original_widelg.jpg)

## Sinopse

Esta versão americana de "The Office" é uma comédia que gira em torno do cotidiano de um escritório. Esta sátira descreve a vida dos funcionários da fábrica de papel Dunder Miffin, situada em Scranton, na Pensilvânia. Entre os personagens está Michael Scout (Steve Carell), o gerente regional da empresa. Solteiro e fanfarrão, ele acredita ser o homem mais bonito do escritório, além de uma fonte de sabedoria e o melhor amigo de seus funcionários. Pam Beesly (Jenna Fischer) é a simpática recepcionista que o tolera. Alguns dos melhores momentos são as conversas que ela tem com o representante de vendas Jim Halpert (John Krasinski); com Dwight Schrute (Rainn Wilson), o arrogante assistente de Michael; e com o auxiliar Ryan Howard, que passa a Michael a visão oficial dos fatos.

## Minha opinião

A melhor ou uma das melhores séries que já assisti. Série bem tranquila de assistir, muito leve e engraçada. Tem momentos que o humor é tão cringe que dá vontade de se contorcer por inteiro enquanto morre de rir. Recomendo que qualquer pessoa assista pelo menos as 2 primeiras temporadas porque vale muito a pena.