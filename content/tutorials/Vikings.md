---
title: "Vikings"
date: 2021-10-24T20:29:03-03:00
draft: false
---

![Vikings](https://assets.b9.com.br/wp-content/uploads/2013/03/vikings4.jpg)

## Sinopse

Vikings segue a vida de Ragnar Lothbrok (Travis Fimmel), o maior guerreiro da sua era. Lider de seu bando, com seus irmãos e sua família, ele ascende ao poder e torna-se Rei da tribo dos vikings. Além de guerreiro implacável, Ragnar segue as tradições nórdicas e é devoto dos deuses. As lendas contam que ele descende diretamente de Odin, o deus da guerra.

## Minha opinião

A série começa muito boa, descobrindo como funciona a cultura dos vikings. Porém a série se extendeu demais, ficando um pouco repetitiva. É uma série densa e é preciso prestar atenção para não se esquecer dos nosmes dos personagens e acontecimentos que se correlacionam. No final eu já não aguentava mais assistir, mas compensaram no último episódio.